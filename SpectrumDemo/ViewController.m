//
//  ViewController.m
//  SpectrumDemo
//
//  Created by Georgij on 27.10.16.
//  Copyright © 2016 Georgii Emeljanow. All rights reserved.
//


#import "ViewController.h"
#import <EZAudio/EZAudio.h>

#define kAudioFilePath [[NSBundle mainBundle] pathForResource:@"ViolinSonata1" ofType:@"m4a"]

@interface ViewController () <EZAudioPlayerDelegate>

@property (strong, nonatomic) EZAudioFile* audioFile;
@property (strong, nonatomic) EZAudioPlayer* player;

@property (weak, nonatomic) IBOutlet EZAudioPlotGL* audioPlotView;
@property (nonatomic, weak) IBOutlet UILabel* filePathLbl;
@property (nonatomic, weak) IBOutlet UISlider* positionSlr;

@property (nonatomic, weak) IBOutlet UISlider* volumeSlr;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AVAudioSession* session = [AVAudioSession sharedInstance];
    NSError* error;
    
    [session setCategory:AVAudioSessionCategoryPlayback error:&error];
    if (error) {
        NSLog(@"Error setting up audio session category: %@", error.localizedDescription);
    }
    
    [session setActive:YES error:&error];
    if (error) {
        NSLog(@"Error setting up audio session active: %@", error.localizedDescription);
    }
    
    //    self.audioPlotView.backgroundColor = [UIColor colorWithRed:0.816 green:0.439 blue:0.255 alpha:1.0];
    self.audioPlotView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    self.audioPlotView.color = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    self.audioPlotView.plotType = EZPlotTypeBuffer;
    self.audioPlotView.shouldFill = YES;
    self.audioPlotView.shouldMirror = YES;
    
    NSLog(@"outputs: %@", [EZAudioDevice outputDevices]);
    
    self.player = [EZAudioPlayer audioPlayerWithDelegate:self];
    self.player.shouldLoop = YES;
    
    [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
    if (error) {
        NSLog(@"Error overriding output to the speaker: %@", error.localizedDescription);
    }
    
    [self setupNotifications];
    
    [self openFileWithFilePathURL:[NSURL fileURLWithPath:kAudioFilePath]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Notifications -
- (void)setupNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(audioPlayerDidChangeAudioFile:)
                                                 name:EZAudioPlayerDidChangeAudioFileNotification
                                               object:self.player];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(audioPlayerDidChangeOutputDevice:)
                                                 name:EZAudioPlayerDidChangeOutputDeviceNotification
                                               object:self.player];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(audioPlayerDidChangePlayState:)
                                                 name:EZAudioPlayerDidChangePlayStateNotification
                                               object:self.player];
}

- (void)audioPlayerDidChangeAudioFile:(NSNotification *)notification {
    EZAudioPlayer *player = [notification object];
    NSLog(@"Player changed audio file: %@", [player audioFile]);
}

- (void)audioPlayerDidChangeOutputDevice:(NSNotification *)notification {
    EZAudioPlayer *player = [notification object];
    NSLog(@"Player changed output device: %@", [player device]);
}

- (void)audioPlayerDidChangePlayState:(NSNotification *)notification {
    EZAudioPlayer *player = [notification object];
    NSLog(@"Player change play state, isPlaying: %i", [player isPlaying]);
}

///
- (void)openFileWithFilePathURL:(NSURL *)filePathURL {
    self.audioFile = [EZAudioFile audioFileWithURL:filePathURL];
    
    self.filePathLbl.text = filePathURL.lastPathComponent;
    self.positionSlr.maximumValue = (float)self.audioFile.totalFrames;
    self.volumeSlr.value = [self.player volume];
    
    self.audioPlotView.plotType = EZPlotTypeBuffer;
    self.audioPlotView.shouldFill = YES;
    self.audioPlotView.shouldMirror = YES;
    
    __weak typeof (self) weakSelf = self;
    [self.audioFile getWaveformDataWithCompletionBlock:^(float **waveformData, int length) {
        [weakSelf.audioPlotView updateBuffer:waveformData[0] withBufferSize:length];
    }];
    
    [self.player setAudioFile:self.audioFile];
}

#pragma mark - EZAudioPlayerDelegate
- (void)audioPlayer:(EZAudioPlayer *)audioPlayer playedAudio:(float **)buffer withBufferSize:(UInt32)bufferSize withNumberOfChannels:(UInt32)numberOfChannels inAudioFile:(EZAudioFile *)audioFile {
    __weak typeof (self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.audioPlotView updateBuffer:buffer[0] withBufferSize:bufferSize];
    });
}

- (void)audioPlayer:(EZAudioPlayer *)audioPlayer updatedPosition:(SInt64)framePosition inAudioFile:(EZAudioFile *)audioFile {
    __weak typeof (self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!weakSelf.positionSlr.touchInside) {
            weakSelf.positionSlr.value = (float)framePosition;
        }
    });
}

- (IBAction)changeVolumeValue:(id)sender {
    float value = [(UISlider *)sender value];
    [self.player setVolume:value];
}

- (IBAction)playAudio:(id)sender {
    if ([self.player isPlaying]) {
        [self.player pause];
    } else {
        if (self.audioPlotView.shouldMirror && (self.audioPlotView.plotType == EZPlotTypeBuffer)) {
            self.audioPlotView.shouldMirror = NO;
            self.audioPlotView.shouldFill = NO;
        }
        [self.player play];
    }
}

- (IBAction)changePositionValue:(id)sender {
    [self.player seekToFrame:(SInt64)[(UISlider *)sender value]];
}

- (void)drawBufferPlot {
    self.audioPlotView.plotType = EZPlotTypeBuffer;
    self.audioPlotView.shouldMirror = NO;
    self.audioPlotView.shouldFill = NO;
}

- (void)drawRollingPlot {
    self.audioPlotView.plotType = EZPlotTypeRolling;
    self.audioPlotView.shouldFill = YES;
    self.audioPlotView.shouldMirror = YES;
}

@end
